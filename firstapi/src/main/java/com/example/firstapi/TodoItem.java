package com.example.firstapi;

import java.io.Serializable;

public class TodoItem implements Serializable {

    private String todo;
    private int priority = 2;

    public TodoItem() {
    }

    public TodoItem(String todo) {
        this.todo = todo;
    }

    public TodoItem(String todo, int priority) {
        this.todo = todo;
        this.priority = priority;
    }

    public String getTodo() {
        return todo;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Todo: [" + todo + ", Prio: " + priority + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }

        final TodoItem other = (TodoItem) obj;
        if (!this.getTodo().equals(other.getTodo()) || this.getPriority() != other.getPriority()) {
            return false;
        }

        return true;
    }
}
