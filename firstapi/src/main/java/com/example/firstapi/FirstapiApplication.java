package com.example.firstapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class FirstapiApplication {

	String property = "unset";

	@RequestMapping("/")
	public String sayHello() {
		return "Hi Esslingen";
	}

	@GetMapping("/hi")
	public String sayHelloAgain() {
		return "Hello folks at HSE";
	}

	@PostMapping("/test/{variable}")
	public String postVariable(@PathVariable String variable) {

		property = variable;

		return property;
	}

	@RequestMapping(value = { "/info", "/info/{var}" }, method = RequestMethod.GET)
	public String outputProperty(@PathVariable(required = false) String var) {
		if (var != null) {
			property = "Information: " + var;
		} else {
			property = "No information given";
		}
		return property;
	}

	public static void main(String[] args) {
		SpringApplication.run(FirstapiApplication.class, args);
	}

}
